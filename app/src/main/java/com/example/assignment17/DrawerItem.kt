package com.example.assignment17

data class DrawerItem(
    val icon:Int,
    val title:Int,
    val notificationCount:String? = null
) {
}