package com.example.assignment17

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assignment17.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    lateinit var toggle : ActionBarDrawerToggle
    lateinit var drawerAdapter:DrawerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setUpRecycler()

        toggle = ActionBarDrawerToggle(this,binding.drawerLayout,R.string.open,R.string.close)
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()


    }

    private fun setUpRecycler()
    {
        binding.navDrawerRecyclerView.apply {
            drawerAdapter = DrawerAdapter()
            drawerAdapter.setData(DrawerItemsList.list)
            adapter = drawerAdapter
            layoutManager = LinearLayoutManager(context)

        }
    }
}