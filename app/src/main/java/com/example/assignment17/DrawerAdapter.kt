package com.example.assignment17

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment17.databinding.DrawerMenuItemBinding

class DrawerAdapter : RecyclerView.Adapter<DrawerAdapter.DrawerViewHolder>() {


    private var drawerItems = listOf<DrawerItem>()

    inner class DrawerViewHolder(var binding: DrawerMenuItemBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun bind() {
            val currentItem = drawerItems[adapterPosition]
            binding.tvTitle.setText(currentItem.title)
            binding.ivIcon.setImageResource(currentItem.icon)
            binding.tvNotificationCount.text = currentItem.notificationCount
        }
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrawerViewHolder {
        return DrawerViewHolder(
            DrawerMenuItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ))
    }

    override fun onBindViewHolder(holder: DrawerViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = drawerItems.size


    fun setData(item:List<DrawerItem>)
    {
        this.drawerItems = item
        notifyDataSetChanged()
    }
}