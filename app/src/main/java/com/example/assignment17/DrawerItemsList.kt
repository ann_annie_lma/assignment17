package com.example.assignment17

object DrawerItemsList {

    val list= listOf<DrawerItem>(
        DrawerItem(R.mipmap.dashboard,R.string.dashboard),
        DrawerItem(R.mipmap.mail,R.string.mail,"15"),
        DrawerItem(R.mipmap.bell,R.string.notifications,"20"),
        DrawerItem(R.mipmap.calendar,R.string.calendar),
        DrawerItem(R.mipmap.statistics,R.string.statistic),
        DrawerItem(R.mipmap.settings,R.string.settings))
}